#include "main_runner_impl.h"

#include "include/cef_parser.h"
//#include "cefclient/common/client_switches.h"

namespace {

	// The default URL to load in a browser window.
	const char default_url[] = "http://www.google.com";

}  // namespace

MainRunnerImpl::MainRunnerImpl(CefRefPtr<CefCommandLine> command_line,
	bool terminate_when_all_windows_closed)
	: command_line_(command_line),
	terminate_when_all_windows_closed_(terminate_when_all_windows_closed),
	initialized_(false),
	shutdown_(false) {
	DCHECK(command_line_.get());

	// Set the main URL.
	/*if (command_line_->HasSwitch(switches::kUrl))
		main_url_ = command_line_->GetSwitchValue(switches::kUrl);
	if (main_url_.empty())*/
	main_url_ = default_url;

	/*if (command_line_->HasSwitch(switches::kBackgroundColor)) {
		// Parse the background color value.
		CefParseCSSColor(command_line_->GetSwitchValue(switches::kBackgroundColor),
			false, background_color_);
	}
	*/
}

MainRunnerImpl::~MainRunnerImpl() {
	// The context must either not have been initialized, or it must have also
	// been shut down.
	DCHECK(!initialized_ || shutdown_);
}

std::string MainRunnerImpl::GetConsoleLogPath() {
	return GetAppWorkingDirectory() + "console.log";
}

std::string MainRunnerImpl::GetMainURL() {
	return main_url_;
}

bool MainRunnerImpl::Initialize(const CefMainArgs& args,
	const CefSettings& settings,
	CefRefPtr<CefApp> application,
	void* windows_sandbox_info) {
	DCHECK(thread_checker_.CalledOnValidThread());
	DCHECK(!initialized_);
	DCHECK(!shutdown_);

	if (!CefInitialize(args, settings, application, windows_sandbox_info))
		return false;

	// Need to create the RootWindowManager after calling CefInitialize because
	// TempWindowX11 uses cef_get_xdisplay().
	//root_window_manager_.reset(
		//new RootWindowManager(terminate_when_all_windows_closed_));

	initialized_ = true;

	return true;
}

void MainRunnerImpl::Shutdown() {
	DCHECK(thread_checker_.CalledOnValidThread());
	DCHECK(initialized_);
	DCHECK(!shutdown_);

	//root_window_manager_.reset();

	CefShutdown();

	shutdown_ = true;
}
