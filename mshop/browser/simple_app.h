// CefApp does all of the work, but it is an abstract base class that needs reference counting implemented;
// thus, we create a dummy class that inherits off of CefApp but does nothing
#pragma once

#include "include/cef_app.h"

class ExampleCefApp : public CefApp
{
public:
	ExampleCefApp()
	{
	}
	virtual ~ExampleCefApp()
	{
	}

private:
	IMPLEMENT_REFCOUNTING(ExampleCefApp);
};