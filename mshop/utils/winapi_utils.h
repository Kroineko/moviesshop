#ifndef WINAPIUTILS_H
#define WINAPIUTILS_H

#include <Windows.h>

// Set the window's user data pointer.
void SetUserDataPtr(HWND hWnd, void* ptr);

// Return the window's user data pointer.
template <typename T>
T GetUserDataPtr(HWND hWnd) {
	return reinterpret_cast<T>(GetWindowLongPtr(hWnd, GWLP_USERDATA));
}

// Set the window's window procedure pointer and return the old value.
WNDPROC SetWndProcPtr(HWND hWnd, WNDPROC wndProc);

//Get perfect pose for main window
POINT* GetMainWindowPerfectPose();

#endif //WINAPIUTILS_H

