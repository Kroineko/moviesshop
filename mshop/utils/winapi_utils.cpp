#include "winapi_utils.h"

#include "../ui/ui_defines.h"
#include <base\cef_logging.h>

void SetUserDataPtr(HWND hWnd, void* ptr) {
	SetLastError(ERROR_SUCCESS);
	LONG_PTR result = ::SetWindowLongPtr(
		hWnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(ptr));
	CHECK(result != 0 || GetLastError() == ERROR_SUCCESS);
}

WNDPROC SetWndProcPtr(HWND hWnd, WNDPROC wndProc) {
	WNDPROC old =
		reinterpret_cast<WNDPROC>(::GetWindowLongPtr(hWnd, GWLP_WNDPROC));
	CHECK(old != NULL);
	LONG_PTR result = ::SetWindowLongPtr(
		hWnd, GWLP_WNDPROC, reinterpret_cast<LONG_PTR>(wndProc));
	CHECK(result != 0 || GetLastError() == ERROR_SUCCESS);
	return old;
}

POINT* GetMainWindowPerfectPose()
{
	int monitor_width = GetSystemMetrics(SM_CXSCREEN);
	int monitor_height = GetSystemMetrics(SM_CYSCREEN);

	POINT* p = new POINT;

	p->x = (monitor_width - MAIN_WINDOW_WIDTH) / 2;
	p->y = (monitor_height - MAIN_WINDOW_HEIGHT) / 2;

	return p;
}