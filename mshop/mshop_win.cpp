#include <Windows.h>

#include "main_runner_impl.h"
#include "message_loop/main_message_loop_std.h"
#include "message_loop/main_message_loop_multithreaded_win.h"
#include "ui/mshop_window_win.h"
#include "ui/ui_defines.h"
#include "utils/winapi_utils.h"

#include "browser/simple_app.h"
#include "include/cef_sandbox_win.h"
#include "include/base/cef_scoped_ptr.h"

#include <memory>

int APIENTRY wWinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPWSTR lpCmdLine,
	int nShowCmd)
{
	CefMainArgs main_args(hInstance);

	void* sandbox_info = NULL;

	// Parse command-line arguments.
	CefRefPtr<CefCommandLine> command_line = CefCommandLine::CreateCommandLine();
	command_line->InitFromString(::GetCommandLineW());

	// Create a ClientApp of the correct type.
	CefRefPtr<ExampleCefApp> app(new ExampleCefApp);

	// Specify CEF global settings here.
	CefSettings settings;	

	int exit_code = CefExecuteProcess(main_args, app.get(), sandbox_info);
	if (exit_code >= 0) {
		// The sub-process has completed so return here.
		return exit_code;
	}

	scoped_ptr<MainRunnerImpl> main_runner(new MainRunnerImpl(command_line, true));

	// Create the main message loop object.
	scoped_ptr<MainMessageLoop> message_loop;
	if (settings.multi_threaded_message_loop)
		message_loop.reset(new MainMessageLoopMultithreadedWin);
	else
		message_loop.reset(new MainMessageLoopStd);

	// Initialize CEF.
	//CefInitialize(main_args, settings, app.get(), sandbox_info);
	main_runner->Initialize(main_args, settings, app, sandbox_info);

	std::unique_ptr<MShopWindowWin> main_window(new MShopWindowWin(hInstance));

	main_window->Create(GetMainWindowPerfectPose()->x,
		GetMainWindowPerfectPose()->y,
		MAIN_WINDOW_WIDTH,
		MAIN_WINDOW_HEIGHT);

	//main_window->CreateBrowser();
	//MSG msg;
	//while (GetMessage(&msg, NULL, 0, 0)) {
	//	TranslateMessage(&msg);
	//	DispatchMessage(&msg);
	//}

	// Run the message loop. This will block until Quit() is called by the
	// RootWindowManager after all windows have been destroyed.
	int result = message_loop->Run();

	main_runner->Shutdown();

	message_loop.reset();
	main_runner.reset();

	return result;
}

