#ifndef MAIN_RUNNER_IMPL_H
#define MAIN_RUNNER_IMPL_H

#include "main_runner.h"

#include "include/base/cef_scoped_ptr.h"
#include "include/base/cef_thread_checker.h"
#include "include/cef_app.h"
#include "include/cef_command_line.h"

class MainRunnerImpl : public MainRunner
{
public:
	MainRunnerImpl(CefRefPtr<CefCommandLine> command_line,
		bool terminate_when_all_windows_closed);

	// MainContext members.
	std::string GetConsoleLogPath() OVERRIDE;
	std::string GetDownloadPath(const std::string& file_name) OVERRIDE;
	std::string GetAppWorkingDirectory() OVERRIDE;
	std::string GetMainURL() OVERRIDE;

	// Initialize CEF and associated main context state. This method must be
	// called on the same thread that created this object.
	bool Initialize(const CefMainArgs& args,
		const CefSettings& settings,
		CefRefPtr<CefApp> application,
		void* windows_sandbox_info);

	// Shut down CEF and associated context state. This method must be called on
	// the same thread that created this object.
	void Shutdown();
private:
	// Allow deletion via scoped_ptr only.
	friend struct base::DefaultDeleter<MainRunnerImpl>;

	~MainRunnerImpl();

	// Returns true if the context is in a valid state (initialized and not yet
	// shut down).
	bool InValidState() const {
		return initialized_ && !shutdown_;
	}

	CefRefPtr<CefCommandLine> command_line_;
	const bool terminate_when_all_windows_closed_;

	// Track context state. Accessing these variables from multiple threads is
	// safe because only a single thread will exist at the time that they're set
	// (during context initialization and shutdown).
	bool initialized_;
	bool shutdown_;

	std::string main_url_;

	// Used to verify that methods are called on the correct thread.
	base::ThreadChecker thread_checker_;

	DISALLOW_COPY_AND_ASSIGN(MainRunnerImpl);
};

#endif //MAIN_RUNNER_IMPL_H