#include "main_runner.h"

namespace {

	MainRunner* singleton = NULL;

}  // namespace

// static
MainRunner* MainRunner::Get() {
	DCHECK(singleton);
	return singleton;
}

MainRunner::MainRunner() {
	DCHECK(!singleton);
	singleton = this;

}

MainRunner::~MainRunner() {
	singleton = NULL;
}