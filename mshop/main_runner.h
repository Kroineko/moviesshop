#ifndef MAIN_RUNNER_H
#define MAIN_RUNNER_H

#include <string>

#include "include/base/cef_ref_counted.h"
#include "include/internal/cef_types_wrappers.h"

class MainRunner
{
public:
	static MainRunner* Get();

	// Returns the full path to the console log file.
	virtual std::string GetConsoleLogPath() = 0;

	// Returns the full path to |file_name|.
	virtual std::string GetDownloadPath(const std::string& file_name) = 0;

	// Returns the app working directory including trailing path separator.
	virtual std::string GetAppWorkingDirectory() = 0;

	// Returns the main application URL.
	virtual std::string GetMainURL() = 0;

protected:
	MainRunner();
	~MainRunner();

private:
	DISALLOW_COPY_AND_ASSIGN(MainRunner);
};


#endif //MAIN_RUNNER_H