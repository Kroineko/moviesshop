#ifndef IMAGEBUTTON_H
#define IMAGEBUTTON_H

#include "base_window.h"

#include <gdiplus.h>

using namespace Gdiplus;

class ImageButton : public BaseWindow
{
public:
	ImageButton(HINSTANCE hInst, CONST WNDCLASSEX* wcx = NULL);
	~ImageButton();
	enum class ButtonState
	{
		normal,
		hovered,
		pushed,
		disabled
	}button_state;

	void SetImage(int resource, ButtonState state);

	HWND GetWindowHandler(){ return handle_; };

	//message handlers
	LRESULT OnCreate(HWND, WPARAM, LPARAM);
	LRESULT OnPaint(HWND , WPARAM , LPARAM );
	LRESULT OnEraseBackgnd(HWND hwnd, WPARAM wParam, LPARAM lParam);
	LRESULT OnMouseMove(HWND hwnd, WPARAM wParam, LPARAM lParam);
	LRESULT OnMouseLeave(HWND hwnd, WPARAM wParam, LPARAM lParam);
	LRESULT OnMouseLButtonDown(HWND hwnd, WPARAM wParam, LPARAM lParam);
	LRESULT OnMouseLButtonUp(HWND hwnd, WPARAM wParam, LPARAM lParam);

private:
	void InitMessageMap();
	void DrawImage(HDC hdc);

	HWND handle_;

	Bitmap* loadImageFromResource(int resource);

	//GDI+
	GdiplusStartupInput gdiplusStartupInput;
	ULONG_PTR gdiplusToken;

	//Imagies
	Bitmap* button_image_nornal_;
	Bitmap* button_image_hovered_;
	Bitmap* button_image_pushed_;

	bool mouse_in;
};


#endif //IMAGEBUTTON_H