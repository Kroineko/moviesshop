#ifndef MAIN_WINDOW_MANAGER_H
#define MAIN_WINDOW_MANAGER_H

#include <set>

#include "include/base/cef_scoped_ptr.h"
#include "include/cef_command_line.h"
#include "mshop_window_win.h"

class MainWindowManager
{
public:
	// If |terminate_when_all_windows_closed| is true quit the main message loop
	// after all windows have closed.
	explicit MainWindowManager(bool terminate_when_all_windows_closed);

	scoped_refptr<MShopWindowWin> CreateMainWindow();

	// Returns the RootWindow associated with the specified browser ID. Must be
	// called on the main thread.
	scoped_refptr<MShopWindowWin> GetWindowForBrowser(int browser_id);

	// Close all existing windows. If |force| is true onunload handlers will not
	// be executed.
	void CloseAllWindows(bool force);

private:

	const bool terminate_when_all_windows_closed_;

	DISALLOW_COPY_AND_ASSIGN(MainWindowManager);
};

#endif //MAIN_WINDOW_MANAGER_H