#include "image_button.h"

#include "../resource.h"

ImageButton::ImageButton(HINSTANCE hInst, CONST WNDCLASSEX* wcx)
	:button_image_nornal_(NULL),
	button_image_hovered_(NULL),
	button_image_pushed_(NULL),
	mouse_in(false)
{

	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

	SetWndClassName(_T("ImageButton"));
	SetWndClassStyle(CS_HREDRAW | CS_VREDRAW);
	SetWndClassCursor(LoadCursor(NULL, IDC_ARROW));
	SetWndClassBckgndBrush((HBRUSH)GetStockObject(DKGRAY_BRUSH));

	SetWindowStyles(WS_CHILD | WS_VISIBLE);


	SetImage(BUTTON_NORMAL, ButtonState::normal);
	SetImage(BUTTON_HOVERD, ButtonState::hovered);
	SetImage(BUTTON_PUSHED, ButtonState::pushed);

	RegisterWindow();

	InitMessageMap();
}

void ImageButton::InitMessageMap()
{
	HandleMessage(WM_CREATE, &ImageButton::OnCreate, this);
	HandleMessage(WM_PAINT, &ImageButton::OnPaint, this);
	HandleMessage(WM_ERASEBKGND, &ImageButton::OnEraseBackgnd, this);
	HandleMessage(WM_MOUSEMOVE, &ImageButton::OnMouseMove, this);
	HandleMessage(WM_MOUSELEAVE, &ImageButton::OnMouseLeave, this);
	HandleMessage(WM_LBUTTONDOWN, &ImageButton::OnMouseLButtonDown, this);
	HandleMessage(WM_LBUTTONUP, &ImageButton::OnMouseLButtonUp, this);
}

LRESULT ImageButton::OnCreate(HWND hwnd, WPARAM wParam, LPARAM lParam)
{
	handle_ = hwnd;

	return DefWindowProc(hwnd, WM_CREATE, wParam, lParam);
}

LRESULT ImageButton::OnPaint(HWND hwnd, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	HDC hdc;

	hdc = BeginPaint(hwnd, &ps);

	DrawImage(hdc);

	EndPaint(hwnd, &ps);

	return 0;
}

LRESULT ImageButton::OnEraseBackgnd(HWND hwnd, WPARAM wParam, LPARAM lParam)
{
	return 0;
}

LRESULT ImageButton::OnMouseMove(HWND hwnd, WPARAM wParam, LPARAM lParam)
{
	button_state = ButtonState::hovered;

	if (!mouse_in)
	{
		mouse_in = true;

		TRACKMOUSEEVENT tme = { sizeof(tme) };
		tme.dwFlags = TME_LEAVE;
		tme.hwndTrack = hwnd;
		TrackMouseEvent(&tme);
	}

	InvalidateRect(hwnd, NULL, FALSE);
	return 0;
}

LRESULT ImageButton::OnMouseLeave(HWND hwnd, WPARAM wParam, LPARAM lParam)
{
	button_state = ButtonState::normal;

	mouse_in = false;

	InvalidateRect(hwnd, NULL, FALSE);
	return 0;
}

LRESULT ImageButton::OnMouseLButtonDown(HWND hwnd, WPARAM wParam, LPARAM lParam)
{
	button_state = ButtonState::pushed;

	InvalidateRect(hwnd, NULL, FALSE);
	return 0;
}

LRESULT ImageButton::OnMouseLButtonUp(HWND hwnd, WPARAM wParam, LPARAM lParam)
{
	if (mouse_in)
		button_state = ButtonState::hovered;
	else
		button_state = ButtonState::normal;

	InvalidateRect(hwnd, NULL, FALSE);
	return 0;
}

void ImageButton::SetImage(int resource, ButtonState state)
{
	switch (state)
	{
	case ImageButton::ButtonState::normal:
		button_image_nornal_ = loadImageFromResource(resource);
		break;
	case ImageButton::ButtonState::hovered:
		button_image_hovered_ = loadImageFromResource(resource);
		break;
	case ImageButton::ButtonState::pushed:
		button_image_pushed_ = loadImageFromResource(resource);
		break;
	case ImageButton::ButtonState::disabled:
		break;
	default:
		break;
	}
}

void ImageButton::DrawImage(HDC hdc)
{
	
	Graphics graphics(hdc);

	RECT clientRect;
	GetClientRect(GetWindowHandler(), &clientRect);

	Bitmap backBufferBitmap(clientRect.right, clientRect.bottom, &graphics);
	Graphics backBuffer(&backBufferBitmap);

	graphics.SetPixelOffsetMode(PixelOffsetModeHalf);
	graphics.SetSmoothingMode(SmoothingModeAntiAlias);
	graphics.SetInterpolationMode(InterpolationModeHighQualityBicubic);

	//background
	backBuffer.FillRectangle(&SolidBrush(Color(64, 64, 64)), Rect(0, 0, 96, 48));

	//image
	switch (button_state)
	{
	case ImageButton::ButtonState::normal:
		backBuffer.DrawImage(button_image_nornal_, 0, 0, 96, 48);
		break;
	case ImageButton::ButtonState::hovered:
		backBuffer.DrawImage(button_image_hovered_, 0, 0, 96, 48);
		break;
	case ImageButton::ButtonState::pushed:
		backBuffer.DrawImage(button_image_pushed_, 0, 0, 96, 48);
		break;
	case ImageButton::ButtonState::disabled:
		break;
	default:
		break;
	}
	
	graphics.DrawImage(&backBufferBitmap, 0, 0, 0, 0, 96, 48, UnitPixel);
	

}

Bitmap* ImageButton::loadImageFromResource(int resource){
	HRSRC hrsrc = FindResource(GetModuleHandle(NULL), MAKEINTRESOURCE(resource), _T("PNG"));
	if (!hrsrc) return 0;
	// "�����������" HGLOBAL - ��. �������� LoadResource
	HGLOBAL hgTemp = LoadResource(GetModuleHandle(NULL), hrsrc);
	DWORD sz = SizeofResource(GetModuleHandle(NULL), hrsrc);
	void* ptrRes = LockResource(hgTemp);
	HGLOBAL hgRes = GlobalAlloc(GMEM_MOVEABLE, sz);
	if (!hgRes) return 0;
	void* ptrMem = GlobalLock(hgRes);
	// �������� ��������� ������
	CopyMemory(ptrMem, ptrRes, sz);
	GlobalUnlock(hgRes);
	IStream *pStream;
	// TRUE �������� ���������� ������ ��� ��������� Release
	HRESULT hr = CreateStreamOnHGlobal(hgRes, TRUE, &pStream);
	if (FAILED(hr))
	{
		GlobalFree(hgRes);
		return 0;
	}
	// ���������� �������� �� IStream
	Bitmap *image = Bitmap::FromStream(pStream);
	pStream->Release();
	return image;
}

ImageButton::~ImageButton()
{
	delete(button_image_nornal_);
	GdiplusShutdown(gdiplusToken);
}