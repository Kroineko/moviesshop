#include "base_window.h"

#include "../utils/winapi_utils.h"

BOOL BaseWindow::RegisterWindow()
{
	WNDCLASSEX wcx;


	wcx.cbSize = sizeof(WNDCLASSEX);							
	wcx.style = CS_HREDRAW | CS_VREDRAW;						
	wcx.lpfnWndProc = BaseWindow::BaseWndProc;				
	wcx.cbClsExtra = 0;										
	wcx.cbWndExtra = 0;									
	wcx.hInstance = hInstance;								
	wcx.hIcon = icon_;				
	wcx.hCursor = cursor_;					
	wcx.hbrBackground = background_brush_;	
	wcx.lpszMenuName = (menu_name_ == _T("") ? NULL : menu_name_);									
	wcx.lpszClassName = (class_name_ == _T("") ? _T("BaseWindow") : class_name_);							
	wcx.hIconSm = small_icon_;			

	// Register the window class. 
	return RegisterWindow(&wcx);
}

BOOL BaseWindow::RegisterWindow(const WNDCLASSEX* wcx)
{
	if (RegisterClassEx(wcx) == 0)
		return FALSE;
	else
		return TRUE;
}

LRESULT CALLBACK BaseWindow::BaseWndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	

	// get the pointer to the window
	BaseWindow* wnd = GetUserDataPtr<BaseWindow*>(hwnd);

	if (uMsg == WM_NCDESTROY)
	{
		//destroy pointer to object
		SetUserDataPtr(hwnd, NULL);
	}
	else if (uMsg == WM_NCCREATE)
	{
		SetUserDataPtr(hwnd, reinterpret_cast<void*>((LPCREATESTRUCT(lParam))->lpCreateParams));
	}
	else if(wnd != NULL)
	{
		auto it = wnd->message_map.find(uMsg);
		if (it != wnd->message_map.end())
			return it->second(hwnd, wParam, lParam);
	}
	

	return DefWindowProc(hwnd, uMsg, wParam, lParam);
}

BOOL BaseWindow::Create(RECT* rect)
{
	// send the this pointer as the window creation parameter
	HWND handle = CreateWindow(class_name_, _T(""), window_styles_, rect->left, rect->top,
		rect->right - rect->left, rect->bottom - rect->top, parent_, (HMENU)command_, hInstance,
		reinterpret_cast<LPVOID>(this));

	return (handle != NULL);
}

BOOL BaseWindow::Create(LONG x, LONG y, LONG width, LONG height)
{
	// send the this pointer as the window creation parameter
	HWND handle = CreateWindow(class_name_, _T(""), window_styles_, x, y,
		width, height, parent_, (HMENU)command_, hInstance,
		reinterpret_cast<LPVOID>(this));

	return (handle != NULL);
}