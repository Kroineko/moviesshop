#include "toolbar_view.h"

ToolbarView::ToolbarView(HINSTANCE hInst, CONST WNDCLASSEX* wcx)
{
	SetWndClassName(_T("ToolbarView"));
	SetWndClassStyle(CS_HREDRAW | CS_VREDRAW);
	SetWndClassMenuName(_T(""));
	SetWndClassCursor(LoadCursor(NULL, IDC_ARROW));
	SetWndClassBckgndBrush((HBRUSH)GetStockObject(DKGRAY_BRUSH));

	SetWindowStyles(WS_CHILD | WS_VISIBLE | WS_EX_COMPOSITED);

	RegisterWindow();

	InitMessageMap();
}

void ToolbarView::InitMessageMap()
{
	HandleMessage(WM_CREATE, &ToolbarView::OnCreate, this);
}

LRESULT	ToolbarView::OnCreate(HWND hwnd, WPARAM wParam, LPARAM lParam)
{
	handle_ = hwnd;

	button_ = std::make_unique<ImageButton>(GetInstance());

	button_->SetWindowParent(hwnd);

	button_->Create(10, 8, 96, 48);

	return DefWindowProc(hwnd, WM_CREATE, wParam, lParam);
}