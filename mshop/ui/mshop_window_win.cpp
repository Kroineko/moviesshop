#include "mshop_window_win.h"

#include "ui_defines.h"

#include "include/cef_browser.h"
#include "include/cef_command_line.h"
#include "include/wrapper/cef_helpers.h"

#include <functional>

MShopWindowWin::MShopWindowWin(HINSTANCE hInst, CONST WNDCLASSEX* wcx):
client_handler_(nullptr)
{
	SetWndClassName(_T("MShopWindow"));
	SetWndClassStyle(CS_HREDRAW | CS_VREDRAW);
	SetWndClassIcon(LoadIcon(NULL, IDI_APPLICATION));
	SetWndClassSmallIcon(NULL);
	SetWndClassMenuName(_T(""));
	SetWndClassCursor(LoadCursor(NULL, IDC_ARROW));
	SetWndClassBckgndBrush((HBRUSH)GetStockObject(WHITE_BRUSH));

	SetWindowStyles(WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_EX_COMPOSITED);

	RegisterWindow();

	InitMessageMap();
}

void MShopWindowWin::CreateBrowser()
{
	CefWindowInfo window_info;

	RECT rect;
	GetClientRect(this->GetWindowHandler(), &rect);

	rect.top = TOOLBAR_HEIGHT;

	window_info.SetAsChild(GetWindowHandler(), rect);

	// SimpleHandler implements browser-level callbacks.
	client_handler_ = new ClientHandler();

	// Specify CEF browser settings here.
	CefBrowserSettings browser_settings;

	std::string url;

	// Check if a "--url=" value was provided via the command-line. If so, use
	// that instead of the default URL.
	CefRefPtr<CefCommandLine> command_line =
		CefCommandLine::GetGlobalCommandLine();
	url = command_line->GetSwitchValue("url");
	if (url.empty())
		url = "http://www.google.com";

	// Create the first browser window.
	CefBrowserHost::CreateBrowser(window_info, client_handler_.get(), url,
		browser_settings, NULL);
}

void MShopWindowWin::InitMessageMap()
{
	
	HandleMessage(WM_PAINT, &MShopWindowWin::OnPaint, this);
	HandleMessage(WM_CLOSE, &MShopWindowWin::OnClose, this);
	HandleMessage(WM_ERASEBKGND, &MShopWindowWin::OnEraseBackgnd, this);
	HandleMessage(WM_CREATE, &MShopWindowWin::OnCreate, this);
	HandleMessage(WM_SIZE,&MShopWindowWin::OnSize, this);
}

LRESULT MShopWindowWin::OnPaint(HWND hwnd, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	HDC hdc;

	hdc = BeginPaint(hwnd, &ps);

	//TextOut(hdc, 0, 0, _T("This is a TEST!!! HUITEST!!!111ONE"), 36);

	EndPaint(hwnd, &ps);

	return 0;
}

LRESULT MShopWindowWin::OnClose(HWND hwnd, WPARAM wParam, LPARAM lParam)
{
	CefRefPtr<CefBrowser> browser = client_handler_.get()->GetBrowser();
	if (browser) 
		// Notify the browser window that we would like to close it. This
		// will result in a call to ClientHandler::DoClose() if the
		// JavaScript 'onbeforeunload' event handler allows it.
		browser->GetHost()->CloseBrowser(false);

	PostQuitMessage(0);
	return 0;
}

LRESULT MShopWindowWin::OnEraseBackgnd(HWND hwnd, WPARAM wParam, LPARAM lParam)
{
	return 0;
}

LRESULT MShopWindowWin::OnCreate(HWND hwnd, WPARAM wParam, LPARAM lParam)
{

	handle_ = hwnd;

	toolbar_view_ = std::make_unique<ToolbarView>(GetInstance());

	RECT rect;
	GetClientRect(hwnd, &rect);

	rect.bottom = TOOLBAR_HEIGHT;

	toolbar_view_->SetWindowParent(hwnd);

	toolbar_view_->Create(&rect);

	return DefWindowProc(hwnd, WM_CREATE, wParam, lParam);
}

LRESULT MShopWindowWin::OnSize(HWND hwnd, WPARAM wParam, LPARAM lParam)
{
	RECT rect;
	GetClientRect(hwnd, &rect);

	SetWindowPos(toolbar_view_->GetWindowHandler(),
		NULL,
		rect.left,
		rect.top,
		rect.right - rect.left,
		TOOLBAR_HEIGHT,
		SWP_NOZORDER);

	if (client_handler_ != nullptr)
	{
		CefRefPtr<CefBrowser> browser = client_handler_.get()->GetBrowser();

		SetWindowPos(browser->GetHost()->GetWindowHandle(),
			NULL,
			rect.left,
			rect.top + TOOLBAR_HEIGHT,
			rect.right - rect.left,
			rect.bottom - rect.top,
			SWP_NOZORDER);

	}
		
	return DefWindowProc(hwnd, WM_CREATE, wParam, lParam);
}
MShopWindowWin::~MShopWindowWin()
{
}