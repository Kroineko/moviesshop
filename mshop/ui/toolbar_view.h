#ifndef TOOLBARVIEW_H
#define TOOLBARVIEW_H

#include "base_window.h"

#include "image_button.h"
#include <memory>

class ToolbarView : public BaseWindow
{

public:
	ToolbarView(HINSTANCE hInst, CONST WNDCLASSEX* wcx = NULL);

	HWND GetWindowHandler(){ return handle_; };

private:
	void InitMessageMap();

	HWND handle_;

	//Message handlers
	LRESULT OnCreate(HWND hwnd, WPARAM wParam, LPARAM lParam);

	//controls
	std::unique_ptr<ImageButton> button_;
};

#endif//TOOLBARVIEW_H
