#ifndef BASEWINDOW_H
#define BASEWINDOW_H

#include <windows.h>
#include <string>
#include <tchar.h>
#include <map>
#include <functional>

using namespace std::placeholders;


class BaseWindow
{
public:

	template <typename T, typename U>
	void HandleMessage(LONG message, T method, U object){
		std::function<LRESULT(HWND, WPARAM, LPARAM)> func_ = std::bind(method, object, _1, _2, _3);
		message_map.emplace(message, func_);
	};

	virtual void SetWndClassStyle(UINT style){ style_ = style; };
	virtual void SetWndClassIcon(HICON icon){ icon_ = icon; };
	virtual void SetWndClassSmallIcon(HICON small_icon){ small_icon_ = small_icon; };
	virtual void SetWndClassCursor(HCURSOR cursor){ cursor_ = cursor; };
	virtual void SetWndClassBckgndBrush(HBRUSH background_brush){ background_brush_ = background_brush; };
	virtual void SetWndClassMenuName(LPWSTR menu_name){ menu_name_ = menu_name; };
	virtual void SetWndClassName(LPWSTR class_name){ class_name_ = class_name; };

	virtual void SetWindowStyles(DWORD styles){ window_styles_ = styles; };
	virtual void SetWindowParent(HWND parent){ parent_ = parent; };
	virtual void SetWindowCommand(DWORD command){ command_ = command; };

	virtual HINSTANCE GetInstance(){ return hInstance; };
	virtual HWND GetWindowHandler() = 0;

	virtual BOOL RegisterWindow();
	virtual BOOL RegisterWindow(const WNDCLASSEX* wcx);

	// static message handler to put in WNDCLASSEX structure
	static LRESULT CALLBACK BaseWndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

	virtual BOOL Create(RECT* rect);
	virtual BOOL Create(LONG x, LONG y, LONG width, LONG height);

	std::map<long, std::function<LRESULT(HWND, WPARAM, LPARAM)>> message_map;


private:
	HINSTANCE hInstance;

	//Window class
	UINT style_;
	HICON icon_;
	HICON small_icon_;
	HCURSOR cursor_;
	HBRUSH background_brush_;
	LPWSTR menu_name_;
	LPWSTR class_name_;
	//<--window class

	//create
	DWORD window_styles_;
	HWND parent_;
	DWORD command_;
	//
};

#endif //BASEWINDOW_H