#ifndef MSHOPWINDOWWIN_H
#define MSHOPWINDOWWIN_H

#include "base_window.h"

#include "toolbar_view.h"
#include "../browser/client_handler.h"
#include <memory>

class MShopWindowWin : public BaseWindow
{
public:
	MShopWindowWin(HINSTANCE hInst, CONST WNDCLASSEX* wcx = NULL);
	~MShopWindowWin();

	void CreateBrowser();

	HWND GetWindowHandler(){ return handle_; };

private:

	void InitMessageMap();

	//Message handlers
	LRESULT OnPaint(HWND hwnd, WPARAM wParam, LPARAM lParam);
	LRESULT OnClose(HWND hwnd, WPARAM wParam, LPARAM lParam);
	LRESULT OnEraseBackgnd(HWND hwnd, WPARAM wParam, LPARAM lParam);
	LRESULT OnCreate(HWND hwnd, WPARAM wParam, LPARAM lParam);
	LRESULT OnSize(HWND hwnd, WPARAM wParam, LPARAM lParam);

	//controls
	std::unique_ptr<ToolbarView> toolbar_view_;
	
	HWND handle_;

	CefRefPtr<ClientHandler> client_handler_;
};

#endif //MSHOPWINDOWWIN_H